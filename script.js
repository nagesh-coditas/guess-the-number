'use strict';

let number = Math.floor(Math.random()*20+1)

let score = 20 ;

let highScore ;

document.querySelector('.score').textContent = score ;

document.querySelector(".check").addEventListener("click",function () {

    let Num = document.querySelector(".guess").value
    let guessedNumber = Number(Num)

    if (!guessedNumber){
        document.querySelector(".message").textContent = " No number"
        console.log(document.querySelector(".message").textContent )
    }

    else if ( guessedNumber > number){
        if (score > 1){
        document.querySelector(".message").textContent = "Too High"
        score--;
        document.querySelector('.score').textContent = score ;
        }else{
            document.querySelector(".message").textContent = "You loose"
        }
    }
    else if ( guessedNumber === number){
        document.querySelector(".message").textContent = "Correct Guess"
        document.querySelector(".number").textContent = number;
        document.querySelector("body").style.backgroundColor = "green";
        highScore = score
        document.querySelector(".highscore").textContent = highScore ;
    }

    else if (guessedNumber < number){
        if (score > 1){
            document.querySelector(".message").textContent = "Too Low"
            score--;
            document.querySelector('.score').textContent = score ;
        }else{
            document.querySelector(".message").textContent = "You loose"
        }
       
    }
    

})

document.querySelector(".again").addEventListener("click",function (){

    document.querySelector(".number").textContent = "?"
    document.querySelector(".message").textContent = "Start Guessing..."
    document.querySelector(".score").textContent = 20
    document.querySelector(".highscore").textContent = score
    document.querySelector("body").style.backgroundColor = "black"

})

